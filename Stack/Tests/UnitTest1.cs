using System.Runtime.Serialization.Formatters.Binary;
using Stack;

namespace Tests;

public class Tests
{
    [Test]
    public void PopTest()
    {
        StackOnLikedList<int> actual = new StackOnLikedList<int>();
        actual.Push(1);
        actual.Pop();
        StackOnLikedList<int> expect = new StackOnLikedList<int>();
        Assert.AreEqual(actual.Count, expect.Count);
    }
    
    [Test]
    public void PushAndPeekTestInt()
    {
        StackOnLikedList<int> actual = new StackOnLikedList<int>();
        actual.Push(1);
        Assert.AreEqual(actual.Peek(), 1);
    }

    [Test]
    public void PushAndPeekTestDouble()
    {
        StackOnLikedList<double> actual = new StackOnLikedList<double>();
        actual.Push(1.5);
        Assert.AreEqual(actual.Peek(), 1.5);
    }

    [Test]
    public void PushAndPeekTestChar()
    {
        StackOnLikedList<char> actual = new StackOnLikedList<char>();
        actual.Push('1');
        Assert.AreEqual(actual.Peek(), '1');
    }

    [Test]
    public void PushAndPeekTestBool()
    {
        StackOnLikedList<bool> actual = new StackOnLikedList<bool>();
        actual.Push(true);
        Assert.AreEqual(actual.Peek(), true);
    }
    
    [Test]
    public void EnumeratorTest()
    {
        StackOnLikedList<int> actual = new StackOnLikedList<int>();
        actual.Push(4);
        actual.Push(3);
        actual.Push(2);
        actual.Push(1);
        int[] expected = { 1, 2, 3, 4 };
        int i = 0;
        foreach (int item in actual)
        {
            Assert.AreEqual(item, expected[i]);
            i++;
        }
    }

    [Test]
    public void CloneTest()
    {
        StackOnLikedList<int> expected = new StackOnLikedList<int>();
        expected.Push(1);
        expected.Push(2);
        expected.Push(3);
        expected.Push(4);
        StackOnLikedList<int> actual = (StackOnLikedList<int>)expected.Clone();
        Assert.AreEqual(expected.Peek(), actual.Peek());
        actual.Push(500);
        Assert.AreEqual(expected.Peek(), actual.Peek());
    }
    
    [Test]
    public void ArithTests()
    {
        StackOnLikedList<int> actual = new StackOnLikedList<int>();

        actual.Push(1);
        actual.Push(2);
        actual.Push(3);
        actual.Push(4);

        int actual1 = (int)StackOnLikedList<int>.GetArithMean(actual);
        int expected = 3;
        int actual2 = (int)StackOnLikedList<int>.GetArithMeanPipeLine(actual); 
        int expected2 = 2;
        Assert.AreEqual(expected, actual1);
        Assert.AreEqual(expected2, actual2);
    }
    
    [Test]
    [Obsolete("Obsolete")]
    public void TestSerrialization()
    {
        Stack<int> steck = new Stack<int>();
        steck.Push(1);
        steck.Push(2);
        steck.Push(3);
        steck.Push(4);

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream fs = new FileStream("stack.bin", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
        formatter.Serialize(fs, steck);
        fs.Close();
        FileStream fd = new FileStream("stack.bin", FileMode.Open);
        Stack<int> expected = (Stack<int>)formatter.Deserialize(fd);
        Assert.AreEqual(String.Join(" ", expected), String.Join(" ", steck));
    }
    
    [Test]
    public void ExceptionTest()
    {
        Assert.Throws<InvalidOperationException>(() => { 
            StackOnLikedList<int> actual = new StackOnLikedList<int>();
            actual.Pop(); });
    }
}
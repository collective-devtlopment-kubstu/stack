namespace Stack;

public interface IStack<T> : IEnumerable<T>
{
    void Push(T x);
    T Pop();
    T Peek();
}